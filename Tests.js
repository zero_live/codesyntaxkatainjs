const Code = class {
  static in(code, sample) {
    return new Code(code, sample)
  }

  #originalCode = null
  #code = null
  #sample = null

  constructor(code, sample) {
    this.#originalCode = code
    this.#code = code
    this.#sample = sample
  }

  isExecutable() {
    while(this.#hasChildLabel()) {
      this.#removeChildLabel()
    }

    const isExecutable = (this.#code.length === 0)
    this.#regenerateCode()
    return isExecutable
  }

  #removeChildLabel() {
    this.#removeSampleFromCode()
  }

  #regenerateCode() {
    this.#code = this.#originalCode
  }

  #removeSampleFromCode() {
    this.#code = this.#code.replace(this.#sample, "")
  }

  #hasChildLabel() {
    return (this.#code.includes(this.#sample))
  }
}

const CodeExtractor = class {
  static retriveFrom({ code, sample }) {
    const characters = code.split("")
    let codeWithSample = ""

    characters.forEach(character => {
      if(sample.includes(character)) {
        codeWithSample += character
      }
    });

    return codeWithSample
  }
}

const Validator = class {
  static areExecutable(codes) {
    return codes.every(code => code.isExecutable())
  }
}

const CodeSyntax = class {
  static #SOVELS_SIMPLE_CODE = "<>"
  static #BRACKETS_SIMPLE_CODE = "[]"
  static #NO_ERRORS_MESSAGE = "No errors found"

  static check(rawCode) {
    const rawSovelsCode = this.#extractSovelsCodeIn(rawCode)
    const rawBracketsCode = this.#extractBracketsCodeIn(rawCode)
    const sovelsCode = Code.in(rawSovelsCode, this.#SOVELS_SIMPLE_CODE)
    const bracketsCode = Code.in(rawBracketsCode, this.#BRACKETS_SIMPLE_CODE)
    const codes = [sovelsCode, bracketsCode]
    
    if (!Validator.areExecutable(codes)) {
      throw new SyntaxError()
    }

    return this.#NO_ERRORS_MESSAGE
  }

  static #extractSovelsCodeIn(rawCode) {
    let code = this.#extractSampleFrom(rawCode, this.#SOVELS_SIMPLE_CODE)

    return code
  }

  static #extractBracketsCodeIn(rawCode) {
    let code = this.#extractSampleFrom(rawCode, this.#BRACKETS_SIMPLE_CODE)

    return code
  }

  static #extractSampleFrom(rawCode, sample) {
    return CodeExtractor.retriveFrom({
      code: rawCode,
      sample: sample
    })
  }
}

describe("CodeSyntax", () => {
  it("throws an error when the code has a syntax error", () => {
    const codeWithSyntaxError = "><"

    const act = () => { CodeSyntax.check(codeWithSyntaxError) }

    expect(act).toThrowAnError(SyntaxError)
  })

  it("responds with no errors found for a simple code", () => {
    const simpleCode = "<>"

    const message = CodeSyntax.check(simpleCode)

    expect(message).toBe("No errors found")
  })

  it("responds with no errors found for a normal code", () => {
    const simpleCode = "<><>"

    const message = CodeSyntax.check(simpleCode)

    expect(message).toBe("No errors found")
  })

  it("responds with no errors found for a complex code", () => {
    const simpleCode = "<><><>"

    const message = CodeSyntax.check(simpleCode)

    expect(message).toBe("No errors found")
  })

  it("responds with no errors found for a simple code []", () => {
    const simpleCode = "[]"

    const message = CodeSyntax.check(simpleCode)

    expect(message).toBe("No errors found")
  })
})